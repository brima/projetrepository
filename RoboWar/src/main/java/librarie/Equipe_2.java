package librarie;

import java.util.ArrayList;
import java.util.Iterator;



public  class Equipe_2  extends Equipe {

	public Robot r1;
	public Robot r2;
	public Robot r3;
	int choix =1;
	
	int v=1;
	
    public Robot choixRobotJoue() {
    //il faut teste que le robot n'est pas détruit par un autre robot advrese
    	choix=1;//réinitialiser 
    	Robot r = null;	
    	Iterator<Robot> iter= getListRobots().iterator();
	if (v==1)
	{
		while (iter.hasNext())
		{
			if(iter.next().getIdRobot()=="r1")
			{
				
				r=r1;
	
			}
			
		}
		v=2;
	}	
	else
	{
		if(v==2)
		{
			while (iter.hasNext())
			{
				if(iter.next().getIdRobot()=="r2")
				{
					
				
					r=r2;
				}
			}
			v=3;
		}
		else
		{
			if(v==3)
			{
				while (iter.hasNext())
				{
					if(iter.next().getIdRobot()=="r3")
					{
						v=1;
						
						
					}
				}
				r=r3;
			}
		}
	}
	return r;
    }

	public int[] choixAction(Robot robot) {
		int[] info ={0,0,0,0,0};
				
		if(choix==1)
		{
			info[1]=1; //detecter
			choix =2;
		}
		else 
		{
			info[1]=2;// deplacer
			info[2]=robot.getPositionRobot().getPositionX();
			info[3]=robot.getPositionRobot().getPositionY();
			boolean trouver=true;
			// choisir la case ou se deplacer en fonction du retour de la detection
			// reste a faire tester la limite de la catre (au niveau du module d'exc)
			while (trouver)
			{
			  if((infosDetectees[1]!=null) && (infosDetectees[1].getContenu()==CaseType.VID))
				{
				  info[4]=infosDetectees[1].getPosition().getPositionX();
				  info[5]=infosDetectees[1].getPosition().getPositionY();
				  trouver=false;
				}
				else
				{
					if((infosDetectees[2]!=null) && (infosDetectees[2].getContenu()==CaseType.VID))
					{
						info[4]=infosDetectees[2].getPosition().getPositionX();
						info[5]=infosDetectees[2].getPosition().getPositionY();
						trouver=false;
					}
					else
					{
						if((infosDetectees[3]!=null) && (infosDetectees[3].getContenu()==CaseType.VID))
						{
							info[4]=infosDetectees[3].getPosition().getPositionX();
							info[5]=infosDetectees[3].getPosition().getPositionY();
							trouver=false;
						}
						else
						{
							if((infosDetectees[4]!=null) && (infosDetectees[4].getContenu()==CaseType.VID))
							{
								info[4]=infosDetectees[4].getPosition().getPositionX();
								info[5]=infosDetectees[4].getPosition().getPositionY();
								trouver=false;
							}
							else // rester dans la mm case (pas de deplacement)
							{
								info[4]=info[2];
								info[5]=info[3];
								trouver=false;	
							}
							
						}
						
					}
				}
			}
			
			choix=1;
		}
		return info;
	
			
	}
	
	
	
	
	//instancier l'equipe2 (equipe former de 3 robot )
	 public Equipe_2() {
		
		//meme arme pour tous les robot robot qui n'attaque pas
		 Armes ar1=new Armes("fusee",0,0); 
		 
        // creation des robots
		
        r1 = new Robot("r21","e2",ar1);
		addRobot(r1);
		
	    r2 = new Robot("r22","e2",ar1);
	    addRobot(r2);
		
		r3 = new Robot("r23","e2",ar1);
		addRobot(r3);
		 
		
	}

	

	
	
	
	
}