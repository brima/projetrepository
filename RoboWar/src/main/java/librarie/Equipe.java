package librarie;

import java.util.ArrayList;
import java.util.Iterator;

import librarie.Robot;

public abstract class Equipe {

	protected String nomEquipe;
	private ArrayList<Robot> listRobots = new ArrayList<Robot>(); 
	private Robot robotJoue;
	public  Case infosDetectees[]=new Case[4];
	
	public String getNomEquipe(){
		return this.nomEquipe;
	}
	
	public ArrayList<Robot> getListRobots(){
		return this.listRobots;
	}
	public final void deleteRobot(Robot robot) {

		Iterator<Robot> it = this.listRobots.iterator();
		int i = 0;
		while (it.hasNext()) {

		if (it.next().equals(robot)) {
		this.listRobots.remove(i);
		}
		i++;

		}
		}

		public final boolean addRobot(Robot robot) {
		/*
		* Cette methode permet d'ajouter un rebot à la liste des rebots de
		* l'equipe, si le nombre de points d'equipement n'est pas depassé
		* le nombre de points d'equipement est supposé 100.
		*/
		int nbrPoitEquipementUtilisee = 0;
		Iterator<Robot> it = this.listRobots.iterator();
		while (it.hasNext()) {

		nbrPoitEquipementUtilisee += it.next().getNbrPoindEquipement();
		}
		if ((nbrPoitEquipementUtilisee + robot.getNbrPoindEquipement()) <= 100) { 
		// 100 est le nombre de points d'action à ne pas depasser
		this.listRobots.add(robot);
		return true;
		} else
		System.out.println("vous avez epuisé le nombre de poind d'action autorisé");
		return false;
		}
	public abstract Robot choixRobotJoue();
	public abstract int[] choixAction(Robot robot);
	


}
