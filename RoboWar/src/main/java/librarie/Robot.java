/*
 * Class Robot
 *
 * 1.0
 *
 * 21/10/2012
 *
 * copyright: M2 ACSIS 2012/2013
 */

package librarie;

public  class Robot {
	private Position positionRobot;
	private String idRobot;
	public String equipe;
	//private int capacite;
	private Armes arme;
	protected int NbrPoindEquipement;
	
	public Robot(){
		//le constructeur par defaut jusqu'ici est vide
	}

	public Robot(String id, String equipe,  Armes arme) {
		this.idRobot = id;
		this.equipe = equipe;
		this.arme = arme;
		this.NbrPoindEquipement = 10 + arme.getNbrPoindEquipement();
	}

	public Position getPositionRobot() {
		return positionRobot;
	}

	public void setPositionRobot(Position positionRobot) {
		this.positionRobot = positionRobot;
	}

	public String getIdRobot() {
		return idRobot;
	}

	public String getEquipe() {
		return equipe;
	}


	public Armes getArme() {
		return arme;
	}

	public int getNbrPoindEquipement() {
		return NbrPoindEquipement;
	}


}
