package librarie;

import java.io.FileNotFoundException;
import java.io.IOException;


public class CreationDesCartes {

	/**
	 * Creation des cartes qui seront utilisées dans le jeux
	 */
	
	public CreationDesCartes() throws FileNotFoundException, IOException{
		
		SerialiserCartes serialse= new SerialiserCartes();
		serialse.addCarte(createCarte1());
		serialse.serialiser();
		
	}
	public void main(String[] args){
		Carte c1= createCarte1();
		for(int i=0; i<9; i++){
			for(int j=0; j<12; j++){
				if(c1.getCarte()[i][j].getContenu()==CaseType.VID)
					System.out.println( "--    ");
				if(c1.getCarte()[i][j].getContenu()==CaseType.MUR)
					System.out.println( "##    ");
				if((c1.getCarte()[i][j].getContenu()==CaseType.DRAPEAU)&&
						(c1.getCarte()[i][j].getNomEquipe()=="equipe1"))
					System.out.println( "D1    ");
				if((c1.getCarte()[i][j].getContenu()==CaseType.DRAPEAU)&&
						(c1.getCarte()[i][j].getNomEquipe()=="equipe2"))
					System.out.println( "D2    ");
				if((c1.getCarte()[i][j].getZoneDepart()=="equipe1")&&
						(c1.getCarte()[i][j].getNomEquipe()=="equipe1"))
					System.out.println( "Z1    ");
				if((c1.getCarte()[i][j].getZoneDepart()=="equipe2")&&
						(c1.getCarte()[i][j].getNomEquipe()=="equipe2"))
					System.out.println( "Z2    ");
			}
		}
	}
	
	private Carte createCarte1(){
		/*
		 *  Creation d'une petite carte de 9 sur 12, avec 20 cases mur,
		 *  zones de departs sur les  deux cotés
		 *  drapaux au milieu des zonez de departs.
		 *  
		 */
		String name="carte1"; // nom de la carte
		Case[][] carte = new Case[9][12]; 
		
		// positionement aleatoire des murs
		for (int i=0; i<20 ; i++){
			int k= selectNbrAleatoire(0,8);
			int j= selectNbrAleatoire(0,11);
			carte[k][j].setContenu(CaseType.MUR);
		
		}
		
		// positionnement des zones de departs sur les cotés
		for (int i=0; i<7 ; i++){
			
			carte[0][i].setContenu(CaseType.VID);	
			carte[0][i].setZoneDepart("equipe1");
			carte[11][i].setContenu(CaseType.VID);	
			carte[11][i].setZoneDepart("equipe2");
		}
		
		//positionement des drapeaux
		carte[0][4].setContenu(CaseType.DRAPEAU);
		carte[0][4].setNomEquipe("equipe1");
		carte[11][4].setContenu(CaseType.DRAPEAU);	
		carte[11][4].setNomEquipe("equipe2");
		
		Carte carte1 = new Carte(name, 2,  carte, 9, 12);
		return carte1;
	}
	
	private int selectNbrAleatoire(int valMin, int valMax) {
		//selection d'une variable aleatoire entre min et max
		return (int) (Math.random() * (valMax - valMin)) + valMin;
	}
	

}
