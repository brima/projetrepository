package librarie;


import java.util.ArrayList;

public class Equipe1 extends Equipe {
	
	boolean conditionDeVictoire[]={true,true,true,true};
	
	//LE CONSTRUCTEUR POUR MON EQUIPE Equipe1
	public void Equipe1(){
		this.nomEquipe="equipe1"; 
		int i=0;  // une variable pour changer le nom des robots
		String nomRobot="robot"+i;
		boolean ajout=false;//la variable qui va contenir le retour de addRobot
		do{
			//AJOUT D'UN ROBOT TOUT EN LE CRÉANT AVEC L'ARME
			ajout=this.addRobot(new Robot(nomRobot,this.nomEquipe,new Armes("monarme",2,1)));
			//DEFINIR LE NOM DU PROCHAIN ROBOT
			i++;
			nomRobot="robot"+i;
		}while(ajout);
		
	}
	@Override
	public Robot choixRobotJoue() {
		return (Robot) getListRobots().get((int) Math.round(Math.random()*this.getListRobots().size()));
	}

	
	public int[] choixAction(Robot robot) {
		int action[]=new int[3];
		int dep=-1;
		boolean[] found={false,false,false,false};
		if (this.infosDetectees[0]!=null){
			if (this.infosDetectees[0].getContenu()==CaseType.VID) {dep=1;}
			if (this.infosDetectees[0].getContenu()==CaseType.ROB) {dep=2;}
			if ((this.infosDetectees[0].getContenu()==CaseType.MUR)||(this.infosDetectees[0].getContenu()==CaseType.DRAPEAU)) {dep=10;}
		}
		if (dep==10){
			if (this.infosDetectees[1].getContenu()==CaseType.VID) {dep=3;}
			if (this.infosDetectees[1].getContenu()==CaseType.ROB) {dep=4;}
			if ((this.infosDetectees[1].getContenu()==CaseType.MUR)||(this.infosDetectees[1].getContenu()==CaseType.DRAPEAU)) {dep=11;}
			}
		if (dep==11){
			if (this.infosDetectees[2].getContenu()==CaseType.VID) {dep=5;}
			if (this.infosDetectees[2].getContenu()==CaseType.ROB) {dep=6;}
			if ((this.infosDetectees[2].getContenu()==CaseType.MUR)||(this.infosDetectees[2].getContenu()==CaseType.DRAPEAU)) {dep=12;}
			}
		if (dep==12){
			if (this.infosDetectees[3].getContenu()==CaseType.VID) {dep=7;}
			if (this.infosDetectees[3].getContenu()==CaseType.ROB) {dep=8;}
			//if (this.infosDetectees[3].getContenu()==CaseType.DRAPEAU) {dep=9;}
			}
		/////////////////////////retourner action et position suivant la valeur de dep
		switch(dep){
		case 1:
			action[0]=2;
			action[1]=this.infosDetectees[0].getPosition().getPositionX();
			action[2]=this.infosDetectees[0].getPosition().getPositionY();
		break;
		case 3:
			action[0]=2;
			action[1]=this.infosDetectees[1].getPosition().getPositionX();
			action[2]=this.infosDetectees[1].getPosition().getPositionY();
		break;
		case 5:
			action[0]=2;
			action[1]=this.infosDetectees[2].getPosition().getPositionX();
			action[2]=this.infosDetectees[2].getPosition().getPositionY();
		break;
		case 7:
			action[0]=2;
			action[1]=this.infosDetectees[3].getPosition().getPositionX();
			action[2]=this.infosDetectees[3].getPosition().getPositionY();
		break;
		case 2:
			action[0]=3;
			action[1]=this.infosDetectees[0].getPosition().getPositionX();
			action[2]=this.infosDetectees[0].getPosition().getPositionY();
		break;
		case 4:
			action[0]=3;
			action[1]=this.infosDetectees[1].getPosition().getPositionX();
			action[2]=this.infosDetectees[1].getPosition().getPositionY();
		break;
		case 6:
			action[0]=3;
			action[1]=this.infosDetectees[2].getPosition().getPositionX();
			action[2]=this.infosDetectees[2].getPosition().getPositionY();
		break;
		case 8:
			action[0]=3;
			action[1]=this.infosDetectees[3].getPosition().getPositionX();
			action[2]=this.infosDetectees[3].getPosition().getPositionY();
		break;		
		}
		
		return action;
	}

	
}
