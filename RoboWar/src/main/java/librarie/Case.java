package librarie;


public class Case {

	// DEFINITION DES VARIABLE DE CLASSE
	private Position position;
	private CaseType contenu;
	private String zoneDepart; 
	private String nomRobot; 
	private String nomEquipe;
	
	//DEFINITION DES CONSTRUCTEURS
	public Case(){
		this.position.setPositionX(0);
		this.position.setPositionY(0);
		this.contenu=CaseType.VID;
		this.zoneDepart="";
		this.nomRobot="";
		this.nomEquipe="";
	}
	public Case( CaseType contenuC, String zoneDepartC, String nomRobotC, String nomEquipeC, Position positionC){
		position=positionC;
		contenu=contenuC;
		zoneDepart=zoneDepartC;
		nomRobot=nomRobotC;
		nomEquipe=nomEquipeC;
	}
	// LES ACCESSEURS
	public String getNomEquipe(){
		return nomEquipe;
	}
	public void setNomEquipe(String nomEquipeA){
		nomEquipe=nomEquipeA;
	}
	public String getNomRobot(){
		return nomRobot;
	}
	public void setNomRobot(String nomRobotA){
		nomRobot=nomRobotA;
	}
	public CaseType getContenu(){
		return contenu;
	}
	public void setContenu(CaseType contenuC){
		contenu=contenuC;
	}
	public String getZoneDepart(){
		return zoneDepart;
	}
	public void setZoneDepart(String zoneDepartC){
		zoneDepart=zoneDepartC;
	}
	public Position getPosition(){
		return this.position;
	}
	public void setPosition(Position positionC){
		this.position.setPositionX(positionC.getPositionX());
		this.position.setPositionY(positionC.getPositionY());
	}
}