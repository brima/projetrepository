package librarie;

public class Carte {
	/*
	 * La Carte
	 */

	String nomCarte;
	private int nbrEquipeMax;
	private Case[][] carte;
	private int dimensionX;
	private int dimensionY;

	public Carte(String nomCarte, int nbrEquipeMax, Case[][] carte, int dimX, int dimY) {
		this.nomCarte = nomCarte;
		this.nbrEquipeMax = nbrEquipeMax;
		this.carte = carte;
		this.dimensionX = dimX;
		this.dimensionY = dimY;
	} 

	public String getNom() {
		return nomCarte;
	}

	public int getNmbrEquipeMax() {
		return nbrEquipeMax;
	}

	public Case[][] getCarte() {
		return carte;
	}

	public int getDimensionX() {
		return dimensionX;
	}

	public int getDimensionY() {
		return dimensionY;
	}
	
	public Case getCase(Position position){
		return this.getCarte()[position.getPositionX()][position.getPositionY()];	
	}
	public void viderCase(Position position){
		int x = position.getPositionX();
		int y = position.getPositionY();
		this.getCarte()[x][y].setContenu(CaseType.VID);
		this.getCarte()[x][y].setNomEquipe(null);
		
	}
	
	public void PositionnerRobot(Robot robot){
		int x = robot.getPositionRobot().getPositionX();
		int y = robot.getPositionRobot().getPositionY();
		
		this.getCarte()[x][y].setContenu(CaseType.VID);
		this.getCarte()[x][y].setNomEquipe(null);
		
	}
	

}
