/*
 * class SerialiserCartes
 *
 * 1.0
 *
 * 24/10/2012
 *
 * copyright: M2 ACSIS 2012/2013
 */

package librarie;

import java.io.*;
import java.util.ArrayList;

/**
 * @const
 * @param ...
 */
public class SerialiserCartes implements Serializable {
	/**
	 * Serialise les strategies dans un fichiers text
	 */
	private ArrayList<Carte> listCartes;

	public SerialiserCartes() {
		listCartes = new ArrayList();

	}

	public void serialiser() throws FileNotFoundException, IOException {

		ObjectOutputStream fluxSerialisation = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(new File(
						"Cartes.txt"))));

		fluxSerialisation.writeObject(this.listCartes);
		fluxSerialisation.close();

	}

	public void addCarte(Carte carte) {
		this.listCartes.add(carte);
	}

	public ArrayList<Carte> getListCartes() {
		return listCartes;
	}

	public ArrayList<Carte> dessirialiserCartes() throws FileNotFoundException,
			IOException, ClassNotFoundException {
		ObjectInputStream fluxDeserialisation = new ObjectInputStream(
				new BufferedInputStream(new FileInputStream(new File(
						"Cartes.txt"))));

		this.listCartes = (ArrayList<Carte>) fluxDeserialisation.readObject();

		fluxDeserialisation.close();
		return this.listCartes;
	}

}
