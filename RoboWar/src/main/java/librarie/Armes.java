package librarie;

public class Armes {
	private String nom;
	private int nbrCoup;
	private int portee;
	private int NbrPoindEquipement;
	

	public Armes(String nom, int nbrCoup, int portee) {
		this.nom = nom;
		this.nbrCoup = nbrCoup;
		this.portee = portee;
		this.NbrPoindEquipement = nbrCoup * 5+ portee * 7;

	}

	public String getNom() {
		return this.nom;
	}

	// Suprimer le coup urilisé
	public void supprimerCoup() {
		this.nbrCoup--;

	}
	
	public int getNbrCoup() {
		return nbrCoup;
	}

	public int getPortee() {
		return portee;
	}

	public int getNbrPoindEquipement() {
		return NbrPoindEquipement;
	}	
	

}
