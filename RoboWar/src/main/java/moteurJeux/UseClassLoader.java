package moteurJeux;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class UseClassLoader {
	public static void main(String[] args) {
		try {
			URLClassLoader loader = new URLClassLoader(new URL[] { new URL(
					"file://C:/Program Files/Java/Libraries/JarRW.jar") });
			Class<?> a1 = loader.loadClass("Master_ACSIS_G01.RoboWar.App");
			System.out.println(a1.getName());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
