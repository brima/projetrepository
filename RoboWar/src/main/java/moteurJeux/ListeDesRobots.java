package moteurJeux;

import java.util.ArrayList;
import java.util.HashMap;

import librarie.Robot;

public class ListeDesRobots {
	private HashMap<Robot, Boolean[][]> mapRobotParcours;
	
	public ListeDesRobots() {
		this.mapRobotParcours = new HashMap<Robot, Boolean[][]>();
	}
	public HashMap<Robot, Boolean[][]> getListeDesRobotsEquipe() {
	return mapRobotParcours;
	}

	public void setListeDesRobotEquipe(
		HashMap<Robot, Boolean[][]> mapRobotParcours) {
	this.mapRobotParcours = mapRobotParcours;
	}
	public void ajouterRobot(Robot contenu ,Boolean[][] etatDesCase) {
		mapRobotParcours.put(contenu, etatDesCase);
	}

}
