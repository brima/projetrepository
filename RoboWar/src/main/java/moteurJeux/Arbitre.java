package moteurJeux;

import java.awt.Robot;
import java.util.ArrayList;

import librarie.Carte;
import librarie.CaseType;
import librarie.Position;

public class Arbitre {
	Carte carte;//elle va contenir la carte du jeux qui adapte selon les actions des robots
	ArrayList<Robot> listeDesRobots; //elle va contenir tous les robots du jeux
	
	
	//le robot qui effectue une attaque appelle l'arbitre pour evaluer les effets
	
	public Arbitre(){
		super();
	}

	
	public void evaluerAttaque(Position position1, Position position2){
			
		Carte carte = null;
		if((position1.getPositionX()==position2.getPositionX())||(position1.getPositionY()==position2.getPositionY()))
		{
			//les attaques ne peuvent etre que suivant une ligne ou un colonne
	
				if(position1.getPositionX()!=position2.getPositionX())
				{
					if(position1.getPositionX()<position2.getPositionX())
					{
						for(int i=position1.getPositionX();i<=position2.getPositionX();i++)
						{
							//dans ce for si on prend la portée des armes limitée à n cases 
							//on utilise n+position1.getPositionX pour sortir de for borne
							//superieur de i
							//l'utilisation de la porté est recommandée
							Position position3= new Position(i,position1.getPositionY());
							if((carte.getCase(position3).getContenu()!= CaseType.VID))
							{
								if(carte.getCase(position3).getContenu() == CaseType.ROB)
								{
									for(Robot r : listeDesRobots)
									{
										if(r.getPositionRobot().equals(position3))
										{
											listeDesRobots.remove(r);
											//à modifier listeDesRobots par la liste officielle
											//un message soit envoyé à la classe contenant cette liste
										}
									}
								}
							}
						}						
					}
					else
					{
						for(int i=position1.getPositionX();i>=position2.getPositionX();i--)
						{
							//dans ce for si on prend la portée des armes limitée à n cases 
							//on utilise n+position1.getPositionX pour sortir de for borne superieur de i
							Position position3= new Position(i,position1.getPositionY());
							if((carte.getCase(position3).getContenu()!= CaseType.VID))
							{
								if(carte.getCase(position3).getContenu() == CaseType.ROB)
								{
									for(Robot r : listeDesRobots)
									{
										if(r.getPositionRobot().equals(position3))
										{
											listeDesRobots.remove(r);
											//à modifier listeDesRobots par la liste officielle
											//un message soit envoyé à la classe contenant cette liste
										}
									}
								}
							}
						}
					}
				}
				else
				{
					if(position1.getPositionY()<=position2.getPositionY())
					{
						for(int i=position1.getPositionY();i<position2.getPositionY();i++)
						{
							//dans ce for si on prend la portée des armes limitée à n cases 
							//on utilise n+position1.getPositionX pour sortir de for borne superieur de i
							Position position3= new Position(position1.getPositionX(), i);
							if((carte.getCase(position3).getContenu()!= CaseType.VID))
							{
								if(carte.getCase(position3).getContenu() == CaseType.ROB)
								{
									for(Robot r : listeDesRobots)
									{
										if(r.getPositionRobot().equals(position3))
										{
											listeDesRobots.remove(r);
											//à modifier listeDesRobots par la liste officielle
											//un message soit envoyé à la classe contenant cette liste
										}
									}
								}
							}
						}						
					}
					else
					{
						for(int i=position1.getPositionY();i>=position2.getPositionY();i--)
						{
							//dans ce for si on prend la portée des armes limitée à n cases 
							//on utilise n+position1.getPositionX pour sortir de for borne superieur de i
							Position position3= new Position(position1.getPositionX(), i);
							if((carte.getCase(position3).getContenu()!= CaseType.VID))
							{
								if(carte.getCase(position3).getContenu() == CaseType.ROB)
								{
									for(Robot r : listeDesRobots)
									{
										if(r.getPositionRobot().equals(position3))
										{
											listeDesRobots.remove(r);
											//à modifier listeDesRobots par la liste officielle
											//un message soit envoyé à la classe contenant cette liste
										}
									}
								}
							}
						}
					}
				}
			}
	}
	public Position deplacer(Position position1, Position position2, int nbrPointAction){
		//l'arbitre considere tous les deplacements sont faits selon x puis selon y
		Position position3;
		int x = position1.getPositionX();
		int y = position1.getPositionY();
		
			if(position1.getPositionX()<position2.getPositionX() && nbrPointAction>0)
			{
				for(int i=position1.getPositionX();i<=position2.getPositionX();i++)
				{
					//
					position3= new Position(i,position1.getPositionY());
					if((carte.getCase(position3).getContenu()!= CaseType.VID))
					{
						if(carte.getCase(position3).getContenu() == CaseType.MUR)
						{
							for(Robot r : listeDesRobots)
							{
								if(r.getPositionRobot().equals(position1))
								{
									listeDesRobots.remove(r);
									//à modifier listeDesRobots par la liste officielle
									//un message soit envoyé à la classe contenant cette liste
								}
							}
						}
					}
					else
					{
						nbrPointAction = nbrPointAction-1;
						x = i;
						if(nbrPointAction==0)
						{
							break;
						}
					}
				}						
			}
			if(position1.getPositionX()>position2.getPositionX() && nbrPointAction>0)
			{
				for(int i=position1.getPositionX();i>=position2.getPositionX();i--)
				{
					//dans ce for si on prend la portée des armes limitée à n cases 
					//on utilise n+position1.getPositionX pour sortir de for borne superieur de i
					position3= new Position(i,position1.getPositionY());
					if((carte.getCase(position3).getContenu()!= CaseType.VID))
					{
						if(carte.getCase(position3).getContenu() == CaseType.MUR)
						{
							for(Robot r : listeDesRobots)
							{
								if(r.getPositionRobot().equals(position1))
								{
									listeDesRobots.remove(r);
									//à modifier listeDesRobots par la liste officielle
									//un message soit envoyé à la classe contenant cette liste
								}
							}
						}
					}
					else
					{
						nbrPointAction = nbrPointAction-1;
						x = i;
						if(nbrPointAction==0)
						{
							break;
						}
					}
				}
			}
		
			if(position1.getPositionY()<position2.getPositionY() && nbrPointAction>0)
			{
				for(int i=position1.getPositionY();i<=position2.getPositionY();i++)
				{
					//dans ce for si on prend la portée des armes limitée à n cases 
					//on utilise n+position1.getPositionX pour sortir de for borne superieur de i
					position3= new Position(position1.getPositionX(), i);
					if((carte.getCase(position3).getContenu()!= CaseType.VID))
					{
						if(carte.getCase(position3).getContenu() == CaseType.MUR)
						{
							for(Robot r : listeDesRobots)
							{
								if(r.getPositionRobot().equals(position1))
								{
									listeDesRobots.remove(r);
									//à modifier listeDesRobots par la liste officielle
									//un message soit envoyé à la classe contenant cette liste
								}
							}
						}
					}
					else
					{
						nbrPointAction = nbrPointAction-1;
						y =i;
						if(nbrPointAction==0)
						{
							break;
						}
					}
				}						
			}
			if(position1.getPositionY()>position2.getPositionY() && nbrPointAction>0)
			{
				for(int i=position1.getPositionY();i>=position2.getPositionY();i--)
				{
					//dans ce for si on prend la portée des armes limitée à n cases 
					//on utilise n+position1.getPositionX pour sortir de for borne superieur de i
					position3= new Position(position1.getPositionX(), i);
					if((carte.getCase(position3).getContenu()!= CaseType.VID))
					{//si la case n'est pas vide donc un mur ou un rebot est present dedans 
						//d'où le robot effectuant le deplacement soit mort
						//dans le cas où un rebot en mouvement qui tue un
						//robot rencontré la methode sera à modifier
						if(carte.getCase(position3).getContenu() == CaseType.MUR)
						{
							for(Robot r : listeDesRobots)
							{
								if(r.getPositionRobot().equals(position1))
								{
									listeDesRobots.remove(r);
									//à modifier listeDesRobots par la liste officielle
									//un message soit envoyé à la classe contenant cette liste
								}
							}
						}
						else
						{
							nbrPointAction = nbrPointAction-1;
							y=i;
							if(nbrPointAction==0)
							{
								break;
							}
						}
					}
				}
			}
			return position3 = new Position(x, y);
		}		

}
