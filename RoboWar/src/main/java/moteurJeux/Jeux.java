package moteurJeux;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import librarie.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

import librarie.Carte;

public class Jeux {
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
final Logger logger = LoggerFactory.getLogger(Jeux.class);
//////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
	/*
	 * 
	 */
	private Carte carteJeux;
	private int nbrToursEffectues;
	private int pointsAction;
	private int nbrEquipes;
	private boolean existgagant; 
	private String equipeJoeu;  // la cles de l'equipe qui joue
	private String equipeAttaquee; // la cles de l'equipe attaquée 
	private Robot robotJoeu;   
	private int pointAction;
	private int[] action = new int[3];  // code de l'action a executer
	private HashMap<String, Equipe> listEquipes; 
	private HashMap<String, ListeDesRobots> mapDesEquipes;
	
	// copie des robots de chaque equipe, faite dans le but de verification.
	private HashMap<String, ArrayList<Robot>> robotDesEquipes;
	
	//conditions de victoire de chaque equipe
	private HashMap<String, int[]> condVectoireEquipes;
	private int pointActionReste;
	
	// le nbr points d'action minimum necessaire pour effectuer une action
	private int minPointAction; 

	
	
	private void attaquer(Position positionRobotadverse) {
		Position position3;
			
				if ((robotJoeu.getPositionRobot().getPositionX() == positionRobotadverse.getPositionX())
						|| robotJoeu.getPositionRobot().getPositionY() == positionRobotadverse.getPositionY()) {
					position3 = atteindrePosition(robotJoeu.getPositionRobot(),
							positionRobotadverse, robotJoeu.getArme().getPortee());
					if (carteJeux.getCase(position3).getContenu()== CaseType.ROB) {
						carteJeux.viderCase(position3);
						//recherche du robot adverse accidenté lui aussi sera mort
						Iterator<String> i =  mapDesEquipes.keySet().iterator();
						while(i.next() != null)
						{
							Iterator<Robot> iRobot = mapDesEquipes.get(i).getListeDesRobotsEquipe().keySet().iterator();
							while(iRobot.next()!=null) 
							{
								if(iRobot.next().getPositionRobot().equals(position3))
								{
									detruireRobot();
								}
							}
						}							
					}
			}
		}


	private void detruireRobot() {
		// TODO Auto-generated method stub
		
	}


	private void deplacer(Position positionRobotadverse) {

	}

	private void detecter() {

	}

	private void chargementCarte(int numeroCarte) {
		// @param: le numero de la carte (1: la periere..)

		ArrayList<Carte> listCartes = new ArrayList<Carte>();
		SerialiserCartes serialiseCartes = new SerialiserCartes();
		try {
			listCartes = serialiseCartes.dessirialiserCartes();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		this.carteJeux = listCartes.get(numeroCarte - 1);

	}

	private void ChargerEquipe(String nomEquipe) {

	}

	private boolean testGagnant(String eq) // parametre equipe qui joue 
    {

			
			eq=equipeJoeu;	
			String eqAt=equipeAttaquee;
			int i=0;
			int j=0;
			boolean parcouru=false;
			/*parcourir la map des conditions de victoir pour recuperer les condition de victoire de l'equipe qui joue */
			
			for(String key:this.condVectoireEquipes.keySet())
			{
				if(key==eq)
				{
				/* selon l'action on teste si cette dernier réalise une condition de victoire */
					/* action : deplacement */
					if (action[1]==2)
					{
						/*  condition: capturer le drapeau adverse */
						if(condVectoireEquipes.get(eq)[1]==1)
						{
							if( (carteJeux.getCarte()[action[2]][action[3]].getContenu()==CaseType.DRAPEAU)&&(carteJeux.getCarte()[action[2]][action[3]].getNomEquipe()!=eq)) // drapeau adeverse capturer
							{
								existgagant=true;
							}
						}
						
				
						/* condition : explorer toute la carte */
						if(condVectoireEquipes.get(eq)[3]==1)
						{
							for(Equipe equi : this.mapDesEquipes.keySet())// parcourir hashmap equipe, listerobot 
							{
								if (equi.getNomEquipe()==eq)  // chercher l'equipe qui joue ds hashmap
								{
									for(i=0;i<100;i++)         //parcourir la matrice associer a chaque robot
										{
											for(j=0;j<100;j++) // pour verifier s'il a parcouru toute la carte
											
											{ 
												parcouru=mapDesEquipes.get(equi).getListeDesRobotsEquipe().get(robotJoeu)[i][j] && parcouru;												
											}
											
										}
									
										if(parcouru==true)
										{
											existgagant=true;
										}
								}
							}
							
						}
					}
					/* l'action : attaquer , condition : detriure robot adverse*/
					if(action[1]==3)
					{
						/* condition : détruire tous les robot adverse*/
						if(condVectoireEquipes.get(eq)[2]==1)
						{
						
							for(String key1:this.listEquipes.keySet())
							{
								if((key1==eqAt)&&(listEquipes.get(eq).getListRobots().size()==0))
								{
								existgagant= true;
								}
								
							}
			
						}
					}
				}
				
			
			}
		
			return existgagant;
	}

	private void PositionerEquipe(String clesEquipe) {

	}
	
	//////debutcode de deplacement
	
	public void deplacement(Position positionDestination) {
		Position position3;
		position3 = nouvellePosition(robotJoeu.getPositionRobot(), positionDestination, pointActionReste);
				if (carteJeux.getCase(position3).getContenu()== CaseType.VID) {
					carteJeux.viderCase(robotJoeu.getPositionRobot());
					robotJoeu.setPositionRobot(position3);
					carteJeux.PositionnerRobot(robotJoeu);
				}
				
				if (carteJeux.getCase(position3).getContenu()== CaseType.DRAPEAU) {
					// traitement selon les objectifs de son eqquipe
					action[1]= position3.getPositionX();
					action[2]=position3.getPositionY();
					if (!testGangant())
					{	///le robot doit etre mort
						detruireRobot(robotJoeu);
					}
				}
	
				if (carteJeux.getCase(position3).getContenu()== CaseType.ROB) {
					detruireRobot(robotJoeu);
					Iterator<String> i =  mapDesEquipes.keySet().iterator();
					while(i.next() != null)
					{
						Iterator<Robot> iRobot = mapDesEquipes.get(i).getListeDesRobotsEquipe().keySet().iterator();
						while(iRobot.next()!=null) 
						{
							if(iRobot.next().getPositionRobot().equals(position3))
							{
								detruireRobot(iRobot.next());
							}
						}
					}						
				}

				if (carteJeux.getCase(position3).getContenu()== CaseType.MUR) {
					detruireRobot(robotJoeu);
				}
	}
	
	public Position atteindrePosition(Position position1, Position position2, int pointAction) {
		Position position3 = new Position(position1.getPositionX(),
				position1.getPositionY());
		//elle prend en entrée les positions du source et destination de l'attaque 
		// les attaques ne peuvent etre que suivant une ligne ou une colonne

		if (position1.getPositionX() < position2.getPositionX()) {
			for (int i = position1.getPositionX(); i <= position2.getPositionX(); i++) {
				// dans ce for si on prend la portée des armes limitée à n cases
				// on utilise n+position1.getPositionX pour sortir de for borne
				// superieur de i
				// l'utilisation de la porté est recommandée
				if (pointAction > 1) {
					pointAction = pointAction - 1;
					position3.setPositionX(i);
					
					if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
						return position3;
					}
				}
			}
		}
		if (position1.getPositionX() > position2.getPositionX()) {
			for (int i = position1.getPositionX(); i >= position2
					.getPositionX(); i--) {

				if (pointAction > 1) {
					pointAction--;
					position3.setPositionX(i);
					if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
						return position3;
					}
				}
			}
		}

		if (position1.getPositionY() < position2.getPositionY()) {
			for (int i = position1.getPositionY(); i < position2.getPositionY(); i++) {

				if (pointAction > 1) {
					pointAction--;
					position3.setPositionY(i);
					if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
						return position3;
					}
				}
			}
		}
		if (position1.getPositionY() > position2.getPositionY()) {
			for (int i = position1.getPositionY(); i >= position2
					.getPositionY(); i--) {
				if (pointAction > 1) {
					pointAction--;
					position3.setPositionY(i);
					if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
						return position3;
					}
				}
			}
		}
		return position3;
	}
	
	
 public Position nouvellePosition(Position position1, Position position2, int pointAction) {
	Position position3 = new Position(position1.getPositionX(), position1.getPositionY());
	// /////////////////////////////////
	// les attaques ne peuvent etre que suivant une ligne ou une colonne
	if (position1.getPositionX() < position2.getPositionX()) {
		for (int i = position1.getPositionX(); i <= position2.getPositionX(); i++) {
			// dans ce for si on prend la portée des armes limitée à n cases
			// on utilise n+position1.getPositionX pour sortir de for borne
			// superieur de i
			// l'utilisation de la porté est recommandée
			if (pointAction > 1) {
				pointAction = pointAction - 1;
				position3.setPositionX(i);
				mapDesEquipes.get(equipeJoeu).getListeDesRobotsEquipe().
				get(robotJoeu)[position3.getPositionX()][position3.getPositionY()]=true;
				if(!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
					return position3;
				}
			}
		}
	}
	if (position1.getPositionX() > position2.getPositionX()) {
		for (int i = position1.getPositionX(); i >= position2
						.getPositionX(); i--) {
			if (pointAction > 1) {
				pointAction--;
				position3.setPositionX(i);
				mapDesEquipes.get(equipeJoeu).getListeDesRobotsEquipe().
				get(robotJoeu)[position3.getPositionX()][position3.getPositionY()]=true;
				if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
					return position3;
				}
			}
		}
	}

	if (position1.getPositionY() < position2.getPositionY()) {
		for (int i = position1.getPositionY(); i < position2.getPositionY(); i++) {
			if (pointAction > 1) {
				pointAction--;
				position3.setPositionY(i);
				mapDesEquipes.get(equipeJoeu).getListeDesRobotsEquipe().
				get(robotJoeu)[position3.getPositionX()][position3.getPositionY()]=true;
				if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
					return position3;
				}
			}
		}
	}
	if (position1.getPositionY() > position2.getPositionY()) {
		for (int i = position1.getPositionY(); i >= position2
			.getPositionY(); i--) {
			if (pointAction > 1) {
				pointAction--;
				position3.setPositionY(i);
				mapDesEquipes.get(equipeJoeu).getListeDesRobotsEquipe().
				get(robotJoeu)[position3.getPositionX()][position3.getPositionY()]=true;
				if (!(carteJeux.getCase(position3).getContenu()== CaseType.VID)) {
					return position3;
				}
			}
		}
	}
	return position3;
 }
	
	
	//////////fin code de deplacement
	
	private void executeAction(int[] action) {
		/*
		 * @param: tableau de trois cases, code de l'action a faire, et une
		 * position( la destination robot, ou la position robot adverse
		 * action[0]: code de l'action
		 * (action[1], action[2]): position 
		 */
		if (action[0] == 1) { // code de la detection
			detecter();
		} else {
			if (action[0] == 2) {// code du deplacement
				
				// position destination de robot
				Position destination = new Position(action[1], action[2]);
				deplacer(destination);
			} else {

				if (action[0] == 3) {// code d'une attaque

					// position robot adverse
					Position positionRobAdverse = new Position(action[1], action[2]);
					attaquer(positionRobAdverse);

				} else {
					this.pointActionReste -= 8;
					System.out.println("vous avez perdu 8 point");

				}
			}
		}
	}

	private boolean existeRobot(String key, Robot robot) {
		return true;

	}

	private Jeux() {
		this.existgagant = false;
		this.nbrToursEffectues = 0;
		this.pointAction = 30;
		Scanner sc = new Scanner(System.in);
		boolean nbrEquipeAcdaptee = false;

		// chargement de la CarteJeux
		System.out.println("Veuillez saisir un numero de la Carte :");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
logger.info("une instance du jeux");
//////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
		int numeroCarte = sc.nextInt();
		chargementCarte(numeroCarte);

		// entrer le nombre des equipes
		while (nbrEquipeAcdaptee == false) {
			System.out
					.println("Veuillez saisir un nombre des equipe qui jouent :");
			nbrEquipes = sc.nextInt();
			if (nbrEquipes > this.carteJeux.getNmbrEquipeMax()) {
				System.out.println("vous pouvez jouer avec"
						+ this.carteJeux.getNmbrEquipeMax() + "au maximum");
			} else
				nbrEquipeAcdaptee = true;
		}

		for (int i = 0; i < nbrEquipes; i++) {
			System.out.println("Veuillez saisir le nom que l'equipe");
			String nomEquipe = sc.nextLine();
			ChargerEquipe(nomEquipe);
		}
		// fin de chargement

		// dedut de jeux
		while (!existgagant) {
			/*
			 * Faire un nouveau tour
			 */
			this.nbrToursEffectues += 1;
			pointActionReste = pointAction;
			for (String key : listEquipes.keySet()) {
				/*
				 * pour toutes les equipes
				 */
				this.equipeJoeu = key;
				if (!existgagant) {
					Robot robot = listEquipes.get(key).choixRobotJoue();
					if (existeRobot(key, robot))
						this.robotJoeu = robot;
					else
						// Si le robot n'existe pas dans l'euqipe
						break;
					while ((pointActionReste > minPointAction) || !existgagant) {
						action = listEquipes.get(key).choixAction(robot);
						executeAction(action);
						testGagnant();
					}

				}

			}

		}

	}

	public static void main(String[] args) {
		
		new Jeux();
		
		

	}

}
