package librairie;

public class MurJeux extends ContenuJeux {
	public MurJeux() {
		super("mur");
	}
	
	public MurJeux(String nom, PositionContenu position) {
		super(nom, position);
	}
}
