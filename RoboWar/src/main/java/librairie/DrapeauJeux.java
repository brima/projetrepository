package librairie;

public class DrapeauJeux {
private Equipe equipe;//en attendant de tri de classe Equipe
	
	public DrapeauJeux(String nom, Equipe equipe, PositionContenu position) {
		super(nom, position);
		this.equipe = equipe;
	}
	
	public Equipe getEquipe() {
		return equipe;
	}
	
	protected void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
	
}
