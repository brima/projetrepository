package librairie;

public class ContenuJeux {
	private String nom;
	private PositionContenu position;
	
	 public ContenuJeux(String nom) {
		 position = new PositionContenu();
		 this.nom = nom;
	 }
	 
	 public ContenuJeux(String nom, PositionContenu pos) {
		 this.nom = nom;
		 this.position = pos;
	 }
		
	public String getNom(){
		return this.nom;
	}
	 public ContenuJeux() {
		this.nom = "contenu";
	 }
	 
	 protected final void setPosition(int x, int y)
	 {
		 this.position.setX(x);
		 this.position.setY(y);
	 }
	 
	 public final PositionContenu getPosition()
	 {
		 return this.position;
	 }

}
