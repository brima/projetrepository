package librairie;

public class PositionContenu {
	private int x;
	private int y;
	public PositionContenu(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public PositionContenu() {
		this(0,0);
	}


	public int getX() {
		return this.x;
	}
	protected void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return this.y;
	}
	protected void setY(int y) {
		this.y = y;
	}
}
