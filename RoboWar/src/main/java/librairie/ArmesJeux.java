package librairie;

public class ArmesJeux {
	private String nom;
	private int portee;
	private int nbrPoindEquipement;
	

	public ArmesJeux(String nom, int portee) {
		this.nom = nom;
		this.portee = portee;
		this.nbrPoindEquipement = portee;
	}
	
	public ArmesJeux() {
		this("armement", 1);
	}

	public String getNom() {
		return this.nom;
	}

	public int getPortee() {
		return this.portee;
	}

	public int getNbrPoindEquipement() {
		return this.nbrPoindEquipement;
	}
	
	public void setPortee(int p)
	{
		this.portee = this.nbrPoindEquipement = p;
	}

}
